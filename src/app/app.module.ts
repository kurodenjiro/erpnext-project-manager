import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { ProjectManagerModule } from './project-manager/project-manager.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProjectManagerModule,
    OAuthModule.forRoot(),
    SharedUIModule,
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
